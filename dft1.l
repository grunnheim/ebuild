
在 ebuild.conf.5 中添加一个 builddir-location，表示构建使用的目录，
当安装软件包时，会在此目录下创建 <所在仓库名称>/<类别名称>/<软件包>-<版本> 目录，称为“构建目录”
  并在此目录下创建 DISTDIR，D，BUILD 和 ENV 目录，分别用于存放下载的文件，安装文件，编译软件包，以及存储 ebuild 环境。
  如果是 ebuild 源码包则不创建 DISTDIR 目录，如果是二进制包，则不创建 D 目录。
  存储 ebuild 环境使用 “变量.sh” 和 “函数.sh” 两个文件

在构建和安装过程中会在 “构建目录” 下创建 T、TMPDIR、HOME 目录，分别用于三个同名变量的值

在 lib/ebuild 中创建一个 default_phase 目录，存放 <函数名称>.sh 文件和 default_<函数名称>.sh 文件，
  这些文件定义一个同名函数

在 lib/ebuild 中创建一个 command 目录，存放 ebuild 可用的外部命令，
在 lib/ebuild 中创建一个 function 目录，存放 ebuild 可用的 shell 函数，
  function 目录下的文件：nonfatal.sh，dest-opt.sh，staging-area.sh，use-list.sh，text-list.sh，
                        ver.sh，get_libdir.sh，inherit.sh，default.sh




ebuild 可用的 shell 函数：
nonfatal

into
exeinto
docinto
diropts
exeopts

docompress	; 全局区域不能调用
dostrip	; 全局区域不能调用

use		; 全局区域不能调用
usev		; 全局区域不能调用
use_with	; 全局区域不能调用
use_enable	; 全局区域不能调用
usex		; 全局区域不能调用
in_iuse	; 全局区域不能调用

has

ver_cut	; 全局区域必须可以调用
ver_rs		; 全局区域必须可以调用
ver_test	; 全局区域必须可以调用

get_libdir
inherit	; 全局区域必须可以调用
default	; 全局区域不能调用


在构建目录下创建 eclass 目录，存放 ECLASS.stack，INHERITED 以及
  IUSE，REQUIRED_USE，PROPERTIES，RESTRICT，BDEPEND，RDEPEND，PDEPEND 和 IDEPEND 文件。
在构建目录下创建 function 目录，存放生成的 shell 函数。


在构建目录下创建 default_phase 目录，将 lib/ebuild/default_phase/<函数名称>.sh 复制到此目录下，
当 eclass 执行 EXPORT_FUNCTIONS 时覆盖对应文件的内容。
EXPORT_FUNCTIONS ()
{
	[ -z "${ECLASS}" ] && return

	echo -e "${1} ()\n{" > #构建目录/default_phase#/${1}.sh
	echo -e "\t${ECLASS}_${1} \$@" >> #构建目录/default_phase#/${1}.sh
	echo "}" >> #构建目录/default_phase#/${1}.sh
}

inherit ()
{
	# 如果已定义 ECLASS 变量则将它的值入栈
	if [ -n "${ECLASS}" ]
	then
		echo -e "${ECLASS}\n$(cat #ECLASS.stack#)" | tr --squeeze-repeats '\n' > #ECLASS.stack#new#
		mv -f #ECLASS.stack#new# #ECLASS.stack#
	fi

	while [ $# -gt 0 ]
	do
		export ECLASS=${1}
		echo ${1} >> #INHERITED#
		source #仓库的eclass目录#/${1}.eclass

		# 累加变量
		echo ${IUSE} >> #IUSE#
		unset -v IUSE
		echo ${REQUIRED_USE} >> #REQUIRED_USE#
		unset -v REQUIRED_USE
		echo ${PROPERTIES} >> #PROPERTIES#
		unset -v PROPERTIES
		echo ${RESTRICT} >> #RESTRICT#
		unset -v RESTRICT
		echo ${BDEPEND} >> #BDEPEND#
		unset -v BDEPEND
		echo ${RDEPEND} >> #RDEPEND#
		unset -v RDEPEND
		echo ${PDEPEND} >> #PDEPEND#
		unset -v PDEPEND
		echo ${IDEPEND} >> #IDEPEND#
		unset -v IDEPEND

		unset -v ECLASS
		shift
	done

	# 如果 ECLASS.stack 不为空则出栈一个值赋给 ECLASS
	if [ -n "$(cat #ECLASS.stack#)" ]
	then
		export ECLASS=$(head -n 1 #ECLASS.stack#)
		tail -n $(expr $(cat #ECLASS.stack# | wc -l) - 1) #ECLASS.stack# > #ECLASS.stack#new#
		mv -f #ECLASS.stack#new# #ECLASS.stack#
	fi
}




