
(defun install-op-cons (depspec* install-root group-list target pkgrlst post-list &optional install-pdep-p)
  (let* (category  pkgname  hxlist  error-lvl1)
    (dolist (hxpkg  hxlist)
      (let (repo  version  form  ebuild-env-vars
            regular-slot  sub-slot  old-pkgrec-list  iuse-effective  use-enable-list
            error-lvl2)
        (dolist (use-enable  use-enable-list)
          (let (rollback-func  new-pkgrec  new-op-list  error-lvl3
                xuankong-list  rebuildp-list  new-post-list)

            ;;
            )
        use-level-end)
        )
      hxpkg-level-end)
    (values error-lvl1 pkgrlst :error)
    ))


* 顺序结构和选择结构中的各行代码位于同一层级
* 若某一层级中添加了循环结构或函数调用，则循环结构和被调函数的函数体称为该层级的子层级
* 一个层级在某次执行过程中的报错用 <错误描述节点> 来描述，它的格式为
  (<层级> <错误信息> <子层级错误描述>)。编码名称：errnode，level，errinfo，sublvldesc
  再定义一个 <错误描述节点> 创建宏：
  (defmacro mk-errnode (level errinfo sublvldesc)
    `(list ,level ,errinfo ,sublvldesc))
* 其中 <层级> 是这个层级中一个或多个变量的值，
  变量的选择隐含了报错所针对的对象，同时也隐含了报错位置所属的层级；
  变量的值代表了这个层级是在哪一次执行中报的错。
  <层级> 如果是多个值，则将值放入一张列表
* <错误信息> 是编码时定义好的枚举值，描述了报错的原因，
  隐含了错误产生的位置，以及是否带有“子层级错误描述”
* 当子层级是循环时 <子层级错误描述> 是一张 <错误描述节点> 列表，当子层级是函数调用时 <子层级错误描述> 是单个 <错误描述节点>



安装<层级>：	depspec* --- 入参 <软件包依赖说明符*>，函数的根层级
		hxpkg --- <软件包定位符>，遍历到某个候选软件包的层级
		use-enable --- 应用标志列表，遍历到某个启用的应用标志列表的层级
		(<目标> <依赖说明符节点*>) --- 遍历目标列表的循环中，遍历到目标的某个 <依赖说明符节点*> 的子层级
		作为 (<目标> <依赖说明符节点*>) 子层级的
		<软件包记录> --- 父层级中，当节点的说明符内容带有阻塞符时，
				遍历到相匹配的某一软件包记录的层级
		作为 use-enable 子层级的
		<软件包记录> --- 父层级中，遍历到某一
				新软件包记录建立强运行反向匹配关系后强运行依赖不满足的软件包(被新软件包阻塞强运行依赖的软件包)
				的层级
		作为 use-enable 子层级的
		<待完成节点> --- 当输入参数“推迟完成列表”为空时，遍历<新推迟完成列表>的循环中，遍历到某个 <待完成节点> 的子层级
		作为 <待完成节点> 子层级的
		<软件包记录> --- 父层级中遍历<悬空<软件包记录>列表>的循环中，遍历到某个 <软件包记录> 的子层级

安装<错误信息>：
	<层级>		<错误信息>		含义					子层级错误描述		备注
	所有层级	t			详见子层级错误描述			有
	depspec*	:not-found		未找到符合要求的软件包			无
	hxpkg		:unsupported-eapi	ebuild 遵循的是不支持的 EAPI		无
			:source-error		拉入 ebuild 或环境脚本错误		无
			:slotdep-mismatch	插槽依赖不匹配				无
			:keywords-error		KEYWORDS 错误				无
			:not-stable		软件包不是稳定版			无
			:nrdep-loop		非强运行循环依赖			无
			:usedep-error		应用标志依赖无法满足或无效		无
	use-enable	:target-conflict	与目标相悖				无
			:pkg_pretend-error	pkg_pretend 错误			无
			:cons-pkgrec-failed	<软件包记录> 构造失败			无
			:unsatisfied-bdep	构建依赖无法满足			节点列表
			:unsatisfied-idep	安装依赖无法满足			节点列表
			:pkg-conflict		软件包冲突				节点列表
						(被新软件包阻塞强运行依赖的软件包无法卸载)
			:unsatisfied-rdep	强运行依赖无法满足			节点列表
			:dopost-error		完成已推迟的软件包安装失败		节点列表
	(<> <>)		:install-failed		安装失败				单节点
			:blocked		阻塞					节点列表
	<软件包记录>	:uninstall-failed	卸载失败				单节点			use-enable, (<> <>)，
														<待完成节点>
	<待完成节点>	:xkerr			软件包悬空				节点列表


卸载<层级>：	pkg-record --- 入参 <软件包记录>，函数的根层级
		rec --- 递归卸载时遍历到某个 <软件包记录> 的层级
卸载<错误信息>：
	<层级>		<错误信息>		含义					子层级错误描述		备注
	所有层级	t			详见子层级错误描述			有
	pkg-record	:not-uninstallable	软件包不可卸载				无
			:target-conflict	与目标相悖				无
			:keep-rev-rdep		不卸载悬空的强运行反向依赖		无
			:recursive-failed	递归卸载失败				节点列表
	rec		:uninstall-failed	卸载失败				单节点





(setf fx #'identity)

(let ((f fx))
(setf fx (lambda (x) (format t "fx+~%") (1+ (funcall f x)))))

(let ((f fx))
(setf fx (lambda (x) (format t "fx++~%") (1+ (funcall f x)))))

(let ((f fx))
(setf fx (lambda (x) (format t "fx+++~%") (1+ (funcall f x)))))

(let ((f fx))
(setf fx (lambda (x) (format t "fx++++~%") (1+ (funcall f x)))))

(let ((f fx))
(setf fx (lambda (x) (format t "fx@~%") (1+ (funcall f x)))))




; pkg_pretend 函数执行前需要增加的环境变量：
ROOT			;调用者传参
EROOT

T
TMPDIR
HOME

EPREFIX		;非二进制包从系统轮廓的“构建配置”文件中找，没有则使用软件包管理器定义的默认值"/"。二进制包则从“环境.sh”文件中找

USE

EBUILD_PHASE		#
EBUILD_PHASE_FUNC	#


