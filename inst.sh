#!/bin/bash

#[caller] create environment
# define default phase
# prepare command
# inherit eclass

# check use flag
# analyze dependency

source $1

pkg_pretend

pkg_setup

src_unpack

src_prepare

src_configure

src_compile

src_test

src_install

pkg_preinst

# merge

pkg_postinst
