EAPI=1
DESCRIPTION="Get the https public key of a site."
SLOT="0"

SRC_URI="http://127.0.0.1/${PN}.sh"
RDEPEND="openssl"

src_unpack() {
	cp ${DISTDIR}/${PN}.sh ./${PN}
}

src_install() {
	dobin ${PN}
}
