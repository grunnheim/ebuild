; (defparameter *bash-prog* ...)
; (defparameter *tar-prog* ...)
; (defparameter *env-prog* ...)

;; 易构建运行时环境中的 PATH 变量（路径结尾没有 "/"）
; (defparameter *env-path* ...)

;; 易构建的安装根目录（结尾没有 "/"）
; (defparameter *pkgmgr-rootpath* ...)

;; builddir-location（结尾没有 "/"）
; (defparameter *builddir-location* ...)

;; 仓库列表
; (defparameter *repo-list* ...)

;; 主机架构
; (defparameter *host-architecture* ...)

;; 创建“构建目录”及其各级父目录，以及子目录时设定的权限，暂定 755
; (defparameter *builddir-mode* #8r755)

;; [TODO] <仓库> 增加一个 <仓库根目录> 成员，编码名称“rootpath”，
;         位置暂定为 <仓库优先级> 后边（追加到末尾）


;; 构造拉入 ebuild 前的运行时环境变量文件
;; 参数：
;;	cname		类别名称
;;	pname		非限定的软件包名称
;;	vstr		版本字符串
;;	filesdir	FILESDIR 的值
;;	distdir	DISTDIR 的值
;;	workdir	WORKDIR 的值
;;	build-type	BUILD_TYPE 的值
;;	make.defaults	当前使用的系统轮廓的 <构建配置>
;; 返回值：
;;	同 #'parse-bash-declare
;; 备注：
;;	* CHOST，CBUILD 和 CTARGET 不设置
(defun cons-script (cname pname vstr filesdir distdir workdir build-type make.defaults)
  (let (rcomp ecomp (mid vstr) vlist
        (env-unset (assoc "ENV_UNSET" make.defaults :test #'string=)))
    (let ((p (position #\- mid :test #'char=)))
      (when p
        (setf rcomp (subseq mid (1+ p))
              mid (subseq mid 0 p))))
    (let ((p (position #\_ mid :test #'char=)))
      (when (char-in-range (char mid (1+ p)) #\0 #\9)
        (setf ecomp (subseq mid 0 p)
              mid (subseq mid (1+ p)))))
    (setf vlist (list (list "-rx" "P" (concatenate 'string pname "-" mid))
                      (list "-rx" "PF" (concatenate 'string pname "-" vstr))
                      (list "-rx" "PN" pname)
                      (list "-rx" "CATEGORY" cname)
                      (list "-rx" "PV" mid)
                      (list "-rx" "PE" (if ecomp ecomp "0"))
                      (list "-rx" "PR" (if rcomp rcomp "r0"))
                      (list "-rx" "PVR" vstr)
                      (list "-rx" "FILESDIR" filesdir)
                      (list "-rx" "DISTDIR" distdir)
                      (list "-x" "WORKDIR" workdir)
                      (list "-rx" "BUILD_TYPE" build-type)))
    (if env-unset
      (setf env-unset (nconc (delete "" (string-split (cdr env-unset) #\Space) :test #'string=)
                             (list "GZIP" "BZIP" "BZIP2" "CDPATH" "GREP_OPTIONS" "GREP_COLOR" "GLOBIGNORE")))
      (setf env-unset (list "GZIP" "BZIP" "BZIP2" "CDPATH" "GREP_OPTIONS" "GREP_COLOR" "GLOBIGNORE")))
    (dolist (m make.defaults)
      (unless (find (car m) env-unset :test #'string=)
        (setf vlist (nconc vlist (list (list "-x" (car m) (cdr m)))))))
    (setf vlist (nconc vlist (list (list "-x" "PATH" <PATH>))))	; PATH 变量
    vlist))



;; 从 ebuild 文件中解析 EAPI
;; 参数：
;;	ebuild	带路径的 ebuild 文件名
;; 返回值：
;;	EAPI 字符串或 nil
(defun parse-eapi-file (ebuild)
  (with-open-file (liu ebuild :direction :input)
    (parse-eapi-stream liu)))

;; 从 ebuild 流中解析 EAPI
;; 参数：
;;	liu	ebuild 流
;; 返回值：
;;	EAPI 字符串或 nil
(defun parse-eapi-stream (liu)
  (do ((line (read-line liu nil :eof) (read-line liu nil :eof)))
    ((eql line :eof))
    (let ((cline (string-trim #(#\Space #\Tab) (car (string-split line #\#)))))
      (when (string/= cline "")
        (if (and (> (length cline) 5) (string= (subseq cline 0 5) "EAPI="))
          (progn
            (setf cline (subseq cline 5))
            (if (or (char= (char cline 0) #\") (char= (char cline 0) #\'))
              (if (and (> (length cline) 2) (char= (char cline 0) (char cline (1- (length cline)))))
                (return-from parse-eapi-stream (subseq cline 1 (1- (length cline))))
                (return-from parse-eapi-stream nil))
              (return-from parse-eapi-stream cline)))
          (return-from parse-eapi-stream nil))))))



;; 生成 inherit 和 EXPORT_FUNCTIONS 函数，
;;	以文件 inherit.sh 的形式保存到 <builddir>/function 目录下
;; 参数：
;;	builddir	软件包的构建目录，结尾没有 "/"
;; 返回值：
;;	nil
(defun gen-inherit (builddir)
  (with-open-file (liu (concatenate 'string builddir "/function/inherit.sh") :direction :output)
    ;; 生成 EXPORT_FUNCTIONS 函数
    (format liu "EXPORT_FUNCTIONS ()~%{~%")
    (format liu "[ -z \"${ECLASS}\" ] && return")
    (format liu "echo -e \"${1} ()\\n{\" > ~A/default_phase/${1}.sh~%" builddir)
    (format liu "echo -e \"\\t${ECLASS}_${1} \\$@\" >> ~A/default_phase/${1}.sh~%" builddir)
    (format liu "echo \"}\" >> ~A/default_phase/${1}.sh~%}~%" builddir)
    ;; 生成 inherit 函数
    (format liu "inherit ()~%{~%")
    (format liu "if [ -n \"${ECLASS}\" ]~%then~%")
    (format liu "echo -e \"${ECLASS}\\n$(cat ~A/eclass/ECLASS.stack)\" | tr --squeeze-repeats '\\n' > ~A/eclass/ECLASS.stack.new~%"
                builddir builddir)
    (format liu "mv -f ~A/eclass/ECLASS.stack.new ~A/eclass/ECLASS.stack~%" builddir builddir)
    (format liu "fi~%")
    (format liu "while [ $# -gt 0 ]~%do~%")
    (format liu "export ECLASS=${1}~%")
    (format liu "echo ${1} >> ~A/eclass/INHERITED~%" builddir)
    (format liu "source ~A/eclass/${1}.eclass~%" builddir)
    (dolist (i '("IUSE" "REQUIRED_USE" "PROPERTIES" "RESTRICT" "BDEPEND" "RDEPEND" "PDEPEND" "IDEPEND"))
      (format liu "echo ${~A} >> ~A/eclass/~A~%" i builddir i)
      (format liu "unset -v ~A~%" i))
    (format liu "unset -v ECLASS~%")
    (format liu "shift~%done~%")
    (format liu "if [ -n \"$(cat ~A/eclass/ECLASS.stack)\" ]~%then~%" builddir)
    (format liu "export ECLASS=$(head -n 1 ~A/eclass/ECLASS.stack)~%" builddir)
    (format liu "tail -n $(expr $(cat ~A/eclass/ECLASS.stack | wc -l) - 1) ~A/eclass/ECLASS.stack > ~A/eclass/ECLASS.stack.new~%"
                builddir builddir builddir)
    (format liu "mv -f ~A/eclass/ECLASS.stack.new ~A/eclass/ECLASS.stack~%" builddir builddir)
    (format liu "fi~%}~%")))




;; 拉入非二进制包的 ebuild，生成 “变量.sh” 和 “函数.sh” 两个文件
;; 参数：
;;	reponame	<仓库名称>
;;	category	<类别名称>
;;	pkgname	<非限定的软件包名称>
;;	vstr		<版本字符串>
;;	form		<形式>
;;	installp	编译完是否安装
;; 返回值：
;;	“变量.sh” 的 bash declare -p 列表，
;;	或 :error，表示错误
;; bash 命令：
;;	"cp <函数名称>.sh <所在仓库名称>/<类别名称>/<软件包>-<版本>/default_phase/
	 declare -p >&2
	 source <所在仓库名称>/<类别名称>/<软件包>-<版本>/ENV/变量.sh
	 source <nonfatal.sh>
	 source <dest-opt.sh>
	 source <text-list.sh>
	 source <ver.sh>
	 source <get_libdir.sh>
	 source 生成的inherit.sh
	 source <ebuild>
	 unset -f inherit EXPORT_FUNCTIONS
	 declare -p
	 [ -z "$(declare -f <迭代15个阶段函数名称>)" ] && source <所在仓库名称>/<类别名称>/<软件包>-<版本>/default_phase/<本次迭代的阶段函数名称>.sh
	 declare -f > <所在仓库名称>/<类别名称>/<软件包>-<版本>/ENV/函数.sh
	"
(defun source-ebuild (reponame category pkgname vstr form installp)
  (let* ((builddir *builddir-location*)
         (repo (find reponame *repo-list* :key #'(lambda (r) (repo-name r)) :test #'string=))
         (ebuild-path (path-join (repo-rootpath repo) category pkgname))
         (ebuild-fname (concatenate 'string pkgname "-" vstr
                                            (if (eql form :ebuild) "" ".src")
                                            ".ebuild")))
    ;; 创建 <所在仓库名称>/<类别名称>/<软件包>-<版本> 目录
    (dolist (i (list reponame category (concatenate 'string pkgname "-" vstr)))
      (setf builddir (concatenate 'string builddir "/" i))
      (unless (uiop:directory-exists-p builddir)
        (sb-posix:mkdir builddir *builddir-mode*)))
    ;; 根据 form 复制 ebuild 或解压缩归档
    (if (eql form :ebuild)
      (with-open-file (src (concatenate 'string ebuild-path "/" ebuild-fname) :direction :input)
        (with-open-file (dst (concatenate 'string builddir "/" ebuild-fname)
                         :direction :output)
          (do ((line (read-line src nil :eof) (read-line src nil :eof)))
            ((eql line :eof))
            (format dst "~A~%" line))))
      (unless (zerop (sb-ext:process-exit-code
                       (sb-ext:run-program *tar-prog* (list "-C" builddir "-xf" "-") :wait t
                                           :input (concatenate 'string ebuild-path "/" ebuild-fname))))
        (return-from source-ebuild :error)))
    ;; 创建 DISTDIR，D，BUILD，ENV，eclass，default_phase，function 目录
    (dolist (i '("DISTDIR" "D" "BUILD" "ENV" "eclass" "default_phase" "function"))
      (unless (uiop:directory-exists-p (concatenate 'string builddir "/" i))
        (sb-posix:mkdir (concatenate 'string builddir "/" i) *builddir-mode*)))
    ;; 生成 inherit 和 EXPORT_FUNCTIONS 命令
    (gen-inherit builddir)
    ;; 构造拉入前 ebuild 运行时环境变量并保存到“变量.sh”
    (let* ((pf (assoc (repo-active-profile repo)
                      (repo-profiles repo) :test #'string=))
           (init-vars (cons-script category pkgname vstr
                                   (path-join (repo-rootpath repo) category pkgname "files" )
                                   (concatenate 'string builddir "/DISTDIR")
                                   (concatenate 'string builddir "/BUILD")
                                   (if installp "source" "buildonly")
                                   (profile-make.defaults pf))))
      (with-open-file (liu (concatenate 'string builddir "/ENV/变量.sh") :direction :output)
        (dolist (v init-vars)
          (format liu "declare ~A ~A" (declare-attribute v) (declare-name v))
          (if (declare-value v)
            (format liu "=~S~%" (declare-value v))
            (format liu "~%")))))

    (let* ((cmd (with-output-to-string (str)
                  ;; 构造 bash 命令
                  (dolist (i '("pkg_pretend.sh" "pkg_setup.sh" "src_unpack.sh" "src_prepare.sh"
                               "src_configure.sh" "src_compile.sh" "src_test.sh" "src_install.sh"
                               "pkg_preinst.sh" "pkg_postinst.sh" "pkg_prerm.sh" "pkg_postrm.sh"
                               "pkg_config.sh" "pkg_info.sh" "pkg_nofetch.sh"))
                    (format str "cp ~A/lib/ebuild/default_phase/~A ~A/default_phase/~%" *pkgmgr-rootpath* i builddir))
                  (format str "declare -p >&2~%")
                  (format str "source ~A/ENV/变量.sh~%" builddir)
                  (dolist (i '("nonfatal.sh" "dest-opt.sh" "text-list.sh" "ver.sh" "get_libdir.sh"))
                    (format str "source ~A/lib/ebuild/function/~A~%" *pkgmgr-rootpath* i))
                  (format str "source ~A/function/inherit.sh~%" builddir)
                  (format str "source ~A/~A~%" builddir ebuild-fname)
                  (format str "unset -f inherit EXPORT_FUNCTIONS~%")
                  (format str "declare -p~%")
                  (dolist (i '("pkg_pretend" "pkg_setup" "src_unpack" "src_prepare"
                               "src_configure" "src_compile" "src_test" "src_install"
                               "pkg_preinst" "pkg_postinst" "pkg_prerm" "pkg_postrm"
                               "pkg_config" "pkg_info" "pkg_nofetch"))
                    (format str "[ -z \"$(declare -f ~A)\" ] && source ~A/default_phase/~A.sh~%" i builddir i))
                  (format str "declare -f > ~A/ENV/函数.sh~%" builddir)))
           ;; 调用 bash（env -i --chdir=<WORKDIR> *bash-prog* --norc --noprofile -c <命令>）
           (proc (sb-ext:run-program *env-prog*
                                     (list "-i" (format nil "--chdir=~A/BUILD" builddir)
                                           *bash-prog* "--norc" "--noprofile" "-c" cmd)
                                     :wait t :output :stream :error :stream))

           empty-env-var-names ebuild-env-vars)
      (unless (zerop (sb-ext:process-exit-code proc))
        (return-from source-ebuild :error))
      ;; 读取 stderr，转换成变量名称列表，并剔除 PATH
      (setf empty-env-var-names (delete "PATH" (mapcar #'(lambda (d) (declare-name d))
                                                       (parse-bash-declare
                                                         (sb-ext:process-error proc)))
                                        :test #'string=))
      ;; 读取 stdout，转换成 bash declare 列表，并剔除空环境中的变量
      (setf ebuild-env-vars (delete-if #'(lambda (v)
                                           (find (declare-name v) empty-env-var-names :test #'string=))
                                       (parse-bash-declare (sb-ext:process-output proc))))
      (sb-ext:process-close proc)
      ;; 处理 INHERITED 变量
      (let ((inh ""))
        (with-open-file (liu (concatenate 'string builddir "/eclass/INHERITED") :direction :input)
          (do ((line (read-line liu nil :eof) (read-line liu nil :eof)))
            ((eql line :eof))
            (setf inh (concatenate 'string inh " " line))))
        (setf ebuild-env-vars (nconc ebuild-env-vars (list (list "-rx" "INHERITED" (string-left-trim #(#\Space) inh))))))
      ;; 应用隐式 RDEPEND 规则
      (unless (find "RDEPEND" ebuild-env-vars :key #'cadr :test #'string=)
        (let ((bdepend (find "BDEPEND" ebuild-env-vars :key #'cadr :test #'string=)))
          (when bdepend
            (setf ebuild-env-vars (nconc ebuild-env-vars (list (list "--" "RDEPEND" (caddr bdepend))))))))
      ;; 处理 eclass 累加变量
      (dolist (var '("IUSE" "REQUIRED_USE" "PROPERTIES" "RESTRICT" "BDEPEND" "RDEPEND" "PDEPEND" "IDEPEND"))
        (let ((var-declare (find var ebuild-env-vars :key #'cadr :test #'string=))
              (eclass-value ""))
          (with-open-file (liu (concatenate 'string builddir "/eclass/" var) :direction :input)
            (do ((line (read-line liu nil :eof) (read-line liu nil :eof)))
              ((eql line :eof)
               (setf eclass-value (string-left-trim #(#\Space) eclass-value)))
              (when (string/= line "")
                (setf eclass-value (concatenate 'string eclass-value " " line)))))
          (when (string/= eclass-value "")
            (if var-declare
              (setf (caddr var-declare) (concatenate 'string (caddr var-declare) " " eclass-value))
              (setf ebuild-env-vars (nconc ebuild-env-vars (list (list "--" var eclass-value))))))))
      ;; 输出 ebuild 运行环境变量到 “变量.sh”，返回变量的 declare 列表
      (with-open-file (dst (concatenate 'string builddir "/ENV/变量.sh") :direction :output :if-exists :supersede)
        (dolist (v ebuild-env-vars)
          (format dst "declare ~A ~A" (declare-attribute v) (declare-name v))
          (if (declare-value v)
            (format dst "=~S~%" (declare-value v))
            (format dst "~%"))))
      ebuild-env-vars)))



;; 拉入二进制包中的 “环境.sh”，生成 “变量.sh” 和 “函数.sh” 两个文件
;; 参数：
;;	reponame	<仓库名称>
;;	category	<类别名称>
;;	pkgname	<非限定的软件包名称>
;;	vstr		<版本字符串>
;;	form		<形式>
;;	root		ROOT 变量的新值（软件包将要安装到的根目录的绝对路径，不以斜杠结尾）
;; 返回值：
;;	“变量.sh” 的 bash declare -p 列表，
;;	或 :error，表示错误
;; 说明：
;;	拉入 “环境.sh” 之后会对以下变量进行重新赋值：
;;	* ROOT 和 EROOT
;;	* T
;;	* TMPDIR
;;	* HOME
;;	* D 和 ED
(defun source-binpkg-env (reponame category pkgname vstr form root)
  (let* ((builddir *builddir-location*)
         (repo (find reponame *repo-list* :key #'(lambda (r) (repo-name r)) :test #'string=))
         (ebuild-path (path-join (repo-rootpath repo) category pkgname))
         (pkg-fname (concatenate 'string pkgname "-" vstr "." form ".ebuild")))
    ;; 创建 <所在仓库名称>/<类别名称>/<软件包>-<版本> 目录
    (dolist (i (list reponame category (concatenate 'string pkgname "-" vstr)))
      (setf builddir (concatenate 'string builddir "/" i))
      (unless (uiop:directory-exists-p builddir)
        (sb-posix:mkdir builddir *builddir-mode*)))
    ;; 创建 DISTDIR，BUILD，ENV，T，TMPDIR 和 HOME 目录
    (dolist (i '("DISTDIR" "BUILD" "ENV" "T" "TMPDIR" "HOME"))
      (sb-posix:mkdir (concatenate 'string builddir "/" i) *builddir-mode*))
    ;; 解压缩归档
    (unless (zerop (sb-ext:process-exit-code (sb-ext:run-program *tar-prog* (list "-C" builddir "-xf" "-") :input (concatenate 'string ebuild-path "/" pkg-fname) :wait t)))
      (return-from source-binpkg-env :error))
    ;;
    (let* ((cmd (with-output-to-string (str)
                  ;; 构造 bash 命令
                  (format str "declare -p >&2~%")
                  (format str "source ~A/环境.sh~%" builddir)
                  (format str "declare -p~%")
                  (format str "declare -f > ~A/ENV/函数.sh~%" builddir)))
           ;; 调用 bash（env -i --chdir=<BUILD> *bash-prog* --norc --noprofile -c <命令>）
           (proc (sb-ext:run-program *env-prog* (list "-i" (format nil "--chdir=~A/BUILD" builddir) *bash-prog* "--norc" "--noprofile" "-c" cmd) :output :stream :error :stream :wait t))

           empty-env-var-names pkg-env-vars)
      (unless (zerop (sb-ext:process-exit-code proc))
        (return-from source-binpkg-env :error))
      ;; 读取 stderr，转换成变量名称列表，并剔除 PATH
      (setf empty-env-var-names (delete "PATH" (mapcar #'(lambda (d) (declare-name d))
                                                       (parse-bash-declare
                                                         (sb-ext:process-error proc)))
                                        :test #'string=))
      ;; 读取 stdout，转换成 bash declare 列表，并剔除空环境中的变量
      (setf pkg-env-vars (delete-if #'(lambda (v)
                                        (find (declare-name v) empty-env-var-names :test #'string=))
                                    (parse-bash-declare (sb-ext:process-output proc))))
      (sb-ext:process-close proc)
      ;; 变量重新赋值
      (dolist (i '("T" "TMPDIR" "HOME"))
        (setf (declare-value (find i pkg-env-vars :key #'(lambda (d) (declare-name d)) :test #'string=))
              (concatenate 'string builddir "/" i)))
      (let* ((eprefix (declare-value (find "EPREFIX" pkg-env-vars :key #'(lambda (d) (declare-name d)) :test #'string=)))
             (eroot-new-value (if (string= eprefix "") root (concatenate 'string root "/" eprefix)))
             (d-new-value (concatenate 'string builddir "/D"))
             (ed-new-value (if (string= eprefix "") d-new-value (concatenate 'string d-new-value "/" eprefix))))
        (setf (declare-value (find "ROOT" pkg-env-vars :key #'(lambda (d) (declare-name d)) :test #'string=)) root
              (declare-value (find "EROOT" pkg-env-vars :key #'(lambda (d) (declare-name d)) :test #'string=)) eroot-new-value
              (declare-value (find "D" pkg-env-vars :key #'(lambda (d) (declare-name d)) :test #'string=)) d-new-value
              (declare-value (find "ED" pkg-env-vars :key #'(lambda (d) (declare-name d)) :test #'string=)) ed-new-value))
      ;; 输出二进制包的运行环境变量到 “变量.sh”，返回变量的 declare 列表
      (with-open-file (dst (concatenate 'string builddir "/ENV/变量.sh") :direction :output :if-exists :supersede)
        (dolist (v pkg-env-vars)
          (format dst "declare ~A ~A" (declare-attribute v) (declare-name v))
          (if (declare-value v)
            (format dst "=~S~%" (declare-value v))
            (format dst "~%"))))
      pkg-env-vars)))



;; 获取软件包的稳定性等级
;; 参数：
;;	keywords	软件包 KEYWORDS 变量的值
;;	arch.list	仓库的 <平台列表>
;; 返回值：
;;	t，:~，:!，nil 或 :error
;;	分别代表稳定版，不稳定版，未知，在此平台上不能工作，
;;	以及 keywords 错误
(defun pkg-stability (keywords arch.list)
  (let ((stability :!) hyphen*)
    (dolist (k (delete "" (string-split keywords #\Space) :test #'string=))
      (let (form arch)
        (if (or (char= (char k 0) #\~) (char= (char k 0) #\-))
          (setf arch (subseq k 1)
                form (if (char= (char k 0) #\~) :~ nil))
          (setf arch k
                form t))
        (if (string= k "-*")
          (if hyphen*
            (return-from pkg-stability :error)	; 多个"-*"
            (setf hyphen* t))
          (if (not (find arch arch.list :test #'string=))
            (return-from pkg-stability :error)	; <平台列表> 中不存在的平台
            (when (string= arch *host-architecture*)
              (setf stability form))))))
    (if (and (eql stability :!) hyphen*)
      nil
      stability)))



;; 计算 IUSE_EFFECTIVE
;; 参数：
;;	iuse		IUSE 值
;;	make.defaults	系统轮廓的 <构建配置>
;; 返回值：
;;	一个应用标志列表
(defun calc-iuse-effective (iuse make.defaults)
  (let (iuse-effective)
    ;; IUSE
    (dolist (f (delete "" (string-split iuse #\Space) :test #'string=))
      (when (char= (char f 0) #\+)
        (setf f (subseq f 1)))
      (setf iuse-effective (nconc iuse-effective (list f))))
    ;; IUSE_IMPLICIT
    (let ((iuse-implicit (assoc "IUSE_IMPLICIT" make.defaults :test #'string=)))
      (when iuse-implicit
        (dolist (f (cdr iuse-implicit))
          (setf iuse-implicit (nconc iuse-implicit (list f))))))
    ;; 折叠应用标志
    (let ((use-expand-implicit (assoc "USE_EXPAND_IMPLICIT" make.defaults :test #'string=))
          (use-expand (assoc "USE_EXPAND" make.defaults :test #'string=))
          (use-expand-unprefixed (assoc "USE_EXPAND_UNPREFIXED" make.defaults :test #'string=)))
      ;; 带前缀的折叠应用标志
      (dolist (v (intersection (cdr use-expand) (cdr use-expand-implicit) :test #'string=))
        (let ((use-expand-values (assoc (concatenate 'string "USE_EXPAND_VALUES_" v) make.defaults :test #'string=))
              (lower_v ([TODO])))
          (dolist (x (cdr use-expand-values))
            (setf iuse-effective (nconc iuse-effective (list (concatenate 'string lower_v "_" x)))))))
      ;; 不带前缀的折叠应用标志
      (dolist (v (intersection (cdr use-expand-unprefixed) (cdr use-expand-implicit) :test #'string=))
        (let ((use-expand-values (assoc (concatenate 'string "USE_EXPAND_VALUES_" v) make.defaults :test #'string=)))
          (when use-expand-values
            (setf iuse-effective (nconc iuse-effective (copy-list (cdr use-expand-values))))))))
    iuse-effective))












