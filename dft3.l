

<说明符>：
(<说明符类型> . <说明符字符串>)
* <说明符类型> --- :all，:any，:=1，:<=1，<应用标志条件组> 或 t
			分别代表全选组，任选组，单选组，至多单选组，应用标志条件组，以及原子说明符
			各种组的 <说明符字符串> 不带最外层括号
* <应用标志条件组> --- (<启用?> . <标志名称>)
  编码名称：usecond，enable?，flag
  (defmacro mk-usecond (enable? flag)
    `(cons ,enable? ,flag))
  (defmacro usecond-enable? (spec)
    `(car ,spec))
  (defmacro usecond-flag (spec)
    `(cdr ,spec))
  [谓词] <说明符类型> 是不是 <应用标志条件组>
  (defmacro spectype-usecond-p (stype)
    `(consp ,stype))


(defmacro make-spec (stype stext)
  `(cons ,stype ,stext))
(defmacro spec-type (spec)
  `(car ,spec))
(defmacro spec-text (spec)
  `(cdr ,spec))


############## TODO ################
1. 将代码中创建和引用 <说明符> 的地方改写成使用宏的方式
####################################

;; [新] 拆分说明符
;; 参数：
;;	spec-string	说明符字符串
;;	atom-spec-p	原子说明符判别函数
;;	src-uri-p	拆分的是 SRC_URI
;; 返回值：
;;	一个 <说明符> 列表或 :error
(defun split-spec (spec-string atom-spec-p src-uri-p)
  (labels (;; 创建非原子说明符
           ;; 参数：
           ;;		group-head	说明符左圆括号之前的部分
           ;;		content		说明符圆括号中的部分
           ;; 创建成功返回一个 <说明符>，
           ;; 说明符不合法则从 split-spec 函数返回 :error
           (cons-group-spec (group-head content)
             (when (every (lambda (c)
                            (char= c #\Space))
                          content)	; 此处判断的逻辑应该是“长度为0或所有字符都是空格”，但长度为0时every返回t，所以去掉长度判断
               (return-from split-spec :error))
             (cond
               ((string= group-head "")
                 (make-spec :all content))
               ((string= group-head "||")
                 (make-spec :any content))
               ((string= group-head "^^")
                 (make-spec :=1 content))
               ((string= group-head "??")
                 (make-spec :<=1 content))
               (t		;应用标志条件组
                 (let ((enable? (char/= (char group-head 0) #\!))
                       flag (len (length group-head)))
                   (when (char/= (char group-head (1- len)) #\?)
                     (return-from split-spec :error))
                   (setf flag (subseq group-head (if enable? 0 1) (1- len)))
                   (unless (use-flag-name-p flag t)
                     (return-from split-spec :error))
                   (make-spec (mk-usecond enable? flag) content)))))
           ;; 给定一个说明符字符串和一个 #\) 的索引，
           ;; 向左查找相匹配的 #\( 并将其索引返回，
           ;; 找不到则从 split-spec 函数返回 :error
           (prev-open-parenthesis (spec-str idx)
             (do ((k 1))
               ((minusp (decf idx))
                (return-from split-spec :error))
               (if (char= (char spec-str idx) #\))
                 (incf k)
                 (when (and (char= (char spec-str idx) #\()
                            (zerop (decf k)))
                   (return-from prev-open-parenthesis idx)))))
           ;; 说明符字符串转 <说明符> 列表
           ;; 转换成功返回一个 <说明符> 列表，
           ;; 如果有某个组说明符不合法
           ;; 则从 split-spec 函数返回 :error
           (parse-spectext (spec-str)
             (let ((ep (position #\Space spec-str :from-end t :test #'char/=))
                   sp tail-spec)
               (when ep
                 (if (char= (char spec-str ep) #\))
                   ;; 非原子{
                   (let ((kp (prev-open-parenthesis spec-str ep)))
                     (setf sp (position #\Space spec-str :from-end t :test #'char= :end kp)
                           sp (if sp (1+ sp) 0)
                           tail-spec (cons-group-spec (subseq spec-str sp kp) (subseq spec-str (1+ kp) ep))))
                   ;; 非原子}
                   ;; 原子{
                   (setf sp (position #\Space spec-str :from-end t :test #'char= :end ep)
                         sp (if sp (1+ sp) 0)
                         tail-spec (make-spec t (subseq spec-str sp (1+ ep))))
                   ;; 原子}
                   )
                 (nconc (parse-spectext (subseq spec-str 0 sp)) (list tail-spec)))))
           )	; labels
    (let ((spec-list (parse-spectext spec-string)))
      ;; 处理 "->"
      (when src-uri-p
        (do ((prev nil iter)
             (iter spec-list (cdr iter))
             (next (cdr spec-list) (cdr next)))
          ((null iter))
          (when (and (eql (spec-type (car iter)) t)
                     (string= (spec-text (car iter)) "->"))
            (if (and (eql (spec-type (car prev)) t)
                     (eql (spec-type (car next)) t))
              (setf (spec-text (car prev)) (concatenate 'string
                                                        (spec-text (car prev))
                                                        " -> "
                                                        (spec-text (car next)))
                    (cdr prev) (cdr next)
                    iter prev
                    next (cdr next))
              (return-from split-spec :error)))))
      ;; 检查原子说明符是否合法
      (dolist (spec spec-list spec-list)
        (when (and (eql (spec-type spec) t)
                   (not (funcall atom-spec-p (spec-text spec))))
          (return-from split-spec :error))))))

;; 拆分说明符
;; 参数：
;;	spec-string	说明符字符串
;;	atom-spec-p	原子说明符判别函数
;;	src-uri-p	拆分的是 SRC_URI
;; 返回值：
;;	一个 <说明符> 列表或 :error
(defun split-spec (spec-string atom-spec-p src-uri-p)
  (let ((len (length spec-string))
        spec-list)
    (labels ((next-close-parenthesis (i)	; 给定一个 #\( 的索引，查找相匹配的 #\) 的索引
               (do ((k 1))
                 ((= (incf i) len))
                 (if (char= (char spec-string i) #\()
                   (incf k)
                   (when (and (char= (char spec-string i) #\)) (zerop (decf k)))
                     (return-from next-close-parenthesis i)))))
             (parse-use-conditional (i enable?)	; 以 i 为标志名称首字符索引解析应用标志条件组，返回 #\) 的索引
               (let ((p (position #\? spec-string :start (1+ i) :test #'char=))
                     flag)
                 (when (or (null p)
                           (= (1+ p) len)
                           (char/= (char spec-string (1+ p)) #\())
                   (return-from split-spec :error))
                 (unless (use-flag-name-p (setf flag (subseq spec-string i p)))
                   (return-from split-spec :error))		; 标志名称不合法
                 (setf i (next-close-parenthesis (1+ p)))
                 (when (or (null i) (= (+ p 2) i))
                   (return-from split-spec :error))
                 (setf spec-list (nconc spec-list (list (cons (cons enable? flag)
                                                              (subseq spec-string (+ p 2) i)))))
                 i)))
      (do ((i 0))
        ((= i len))
        (case (char spec-string i)
          (#\Space (incf i))
          (#\(
            (let ((j (next-close-parenthesis i)))
              (when (or (null j) (= (1+ i) j))
                (return-from split-spec :error))
              (setf spec-list (nconc spec-list (list (cons :all (subseq spec-string (1+ i) j)))))
              (when (and (< (setf i (1+ j)) len)
                         (char/= (char spec-string i) #\Space))
                (return-from split-spec :error))))	; 和下一个说明符之间没有空格
          ((#\| #\^ #\?)
            (let ((j (+ i 2)) (c (char spec-string i)))
              (if (and (< j len)
                       (char= c (char spec-string (1+ i)))
                       (char= (char spec-string j) #\())
                (setf i j
                      j (next-close-parenthesis j))
                (return-from split-spec :error))
              (when (or (null j) (= (1+ i) j))
                (return-from split-spec :error))
              (setf spec-list (nconc spec-list (list (cons (case c
                                                             (#\| :any)
                                                             (#\^ :=1)
                                                             (#\? :<=1))
                                                           (subseq spec-string (1+ i) j)))))
              (when (and (< (setf i (1+ j)) len)
                         (char/= (char spec-string i) #\Space))
                (return-from split-spec :error))))	; 和下一个说明符之间没有空格
          (#\!
            (when (= (1+ i) len)
              (return-from split-spec :error))
            (when (and (< (setf i (1+ (parse-use-conditional (1+ i) nil)))
                          len)
                       (char/= (char spec-string i) #\Space))
              (return-from split-spec :error)))	; 和下一个说明符之间没有空格
          (t
            (let ((pos-qm (position #\? spec-string :start (1+ i) :test #'char=))
                  (pos-space (position #\Space spec-string :start (1+ i) :test #'char=)))
              (if (and pos-qm
                       (or (null pos-space)
                           (< pos-qm pos-space)))
                       ;; [TODO] 此处判断是原子还是条件组的方法有缺陷，
                       ;;        例如："类别1/软件包1[标志?]"
                ;; 应用标志条件组{
                (when (and (< (setf i (1+ (parse-use-conditional i t))) len)
                           (char/= (char spec-string i) #\Space))
                  (return-from split-spec :error))	; 和下一个说明符之间没有空格
                ;; }应用标志条件组
                ;; 原子说明符{
                (let ((j (position #\Space spec-string :start (1+ i) :test #'char=)))
                  (setf spec-list (nconc spec-list (list (cons t (subseq spec-string i j)))))
                  (if j
                    (setf i j)
                    (setf i len)))
                ;; }原子说明符
                ))))))
    ;; 处理 "->"
    (do* ((prev nil iter)
          (iter spec-list (cdr iter))
          (next (cdr iter) (cdr next)))
      ((null iter))
      (when (and (eql (caar iter) t)
                 (string= (cdar iter) "->"))
        (if (and src-uri-p
                 (eql (caar prev) t)
                 (eql (caar next) t))
          (setf (cdar prev) (concatenate 'string (cdar prev) " -> " (cdar next))
                (cdr prev) (cdr next)
                iter prev
                next (cdr next))
          (return-from split-spec :error))))
    ;; 检查原子说明符是否合法
    (dolist (spec spec-list spec-list)
      (when (and (eql (car spec) t)
                 (not (funcall atom-spec-p (cdr spec))))
        (return-from split-spec :error)))))



;; 计算启用的应用标志列表
;; 参数：
;;	required-use	软件包的 REQUIRED_USE
;;	iuse-effective	软件包的 IUSE_EFFECTIVE
;;	use+		必须启用的应用标志列表
;;	use-		必须禁用的应用标志列表
;;	iuse		软件包的 IUSE
;; 返回值：
;;	一个由启用的应用标志列表组成的列表(非空)，
;;	或 :not-effective，表示
;;	required-use 中有不在 iuse-effective 中的标志，
;;	或 :not-matched，表示应用标志不匹配
;;	或 :error，表示 REQUIRED_USE 不合法
(defun calc-use-enable (required-use iuse-effective use+ use- iuse)
  (let ((iuse-enable (mapcan #'(lambda (flag)
                                 (when (char= (char flag 0) #\+)
                                   (list (subseq flag 1))))
                             (delete "" (string-split iuse #\Space) :test #'string=))))
    (labels (;; 将一个说明符字符串拆分成 <说明符> 列表，
             ;; 遇到错误则从 calc-use-enable 函数返回 :error
             (split-required-use (req)
               (let ((rlist (split-spec req #'(lambda (a)
                                                (or (use-flag-name-p a)
                                                    (and (char= (char a 0) #\!)
                                                         (use-flag-name-p (subseq a 1)))))
                                        nil)))
                 (if (eql rlist :error)
                   (return-from calc-use-enable :error)
                   rlist)))
             ;; 计算任选组应用标志约束
             ;; 参数：
             ;;	rlist		一张 <说明符> 列表，表示任选组的成员
             ;;	ulist+		必须启用的应用标志列表组成的列表
             ;;	ulist-		必须禁用的应用标志列表组成的列表
             ;;	match?		整个任选组必须匹配(t)还是必须不匹配(nil)
             ;;	tp		任选组的成员是否可以全部不匹配(仅自身递归时需要考虑，外部调用时必须是 nil)
             ;; 遇到不在 IUSE_EFFECTIVE 中的标志返回：
             ;; * :not-effective
             ;; * 相关标志名称
             ;; 如果没有能匹配的组合则返回：
             ;; * :not-matched
             ;; * 最后一个不能匹配的标志名称
             ;; 否则返回：
             ;; * 必须启用的应用标志列表组成的列表
             ;; * 必须禁用的应用标志列表组成的列表
             (anyof-group-constraints (rlist ulist+ ulist- match? &optional tp)
               (let (t-ulist+ t-ulist-
                     nil-ulist+ nil-ulist-
                     mismatched-flag
                     group-ulist+ group-ulist-)
                 (dotimes (i (length ulist+))
                   (multiple-value-bind (node-ulist+ node-ulist-) (use-state-constraints
                                                                    (list (car rlist))
                                                                    (nth i ulist+)
                                                                    (nth i ulist-)
                                                                    match?)
                     (if (listp node-ulist+)
                       (setf t-ulist+ (nconc t-ulist+ node-ulist+)
                             t-ulist- (nconc t-ulist- node-ulist-))
                       (if (eql node-ulist+ :not-effective)
                         (return-from anyof-group-constraints (values :not-effective node-ulist-))
                         (setf mismatched-flag node-ulist-)))))
                 (when (or (cdr rlist) tp)
                   (dotimes (i (length ulist+))
                     (multiple-value-bind (node-ulist+ node-ulist-) (use-state-constraints
                                                                      (list (car rlist))
                                                                      (nth i ulist+)
                                                                      (nth i ulist-)
                                                                      (not match?))
                       (if (listp node-ulist+)
                         (setf nil-ulist+ (nconc nil-ulist+ node-ulist+)
                               nil-ulist- (nconc nil-ulist- node-ulist-))
                         (if (eql node-ulist+ :not-effective)
                           (return-from anyof-group-constraints (values :not-effective node-ulist-))
                           (setf mismatched-flag node-ulist-))))))
                 (unless (or t-ulist+ nil-ulist+)
                   (return-from anyof-group-constraints (values :not-matched mismatched-flag)))
                 (if (cdr rlist)
                   (progn
                     (multiple-value-bind (branch-ulist+ branch-ulist-) (anyof-group-constraints
                                                                          (cdr rlist)
                                                                          t-ulist+
                                                                          t-ulist-
                                                                          match?
                                                                          t)
                       (if (listp branch-ulist+)
                         (setf group-ulist+ branch-ulist+
                               group-ulist- branch-ulist-)
                         (if (eql branch-ulist+ :not-effective)
                           (return-from anyof-group-constraints (values :not-effective branch-ulist-))
                           (setf mismatched-flag branch-ulist-))))
                     (multiple-value-bind (branch-ulist+ branch-ulist-) (anyof-group-constraints
                                                                          (cdr rlist)
                                                                          nil-ulist+
                                                                          nil-ulist-
                                                                          match?
                                                                          tp)
                       (if (listp branch-ulist+)
                         (setf group-ulist+ (nconc group-ulist+ branch-ulist+)
                               group-ulist- (nconc group-ulist- branch-ulist-))
                         (if (eql branch-ulist+ :not-effective)
                           (return-from anyof-group-constraints (values :not-effective branch-ulist-))
                           (setf mismatched-flag branch-ulist-)))))
                   (setf group-ulist+ (nconc t-ulist+ nil-ulist+)
                         group-ulist- (nconc t-ulist- nil-ulist-)))
                 (if group-ulist+
                   (values group-ulist+ group-ulist-)
                   (values :not-matched mismatched-flag))))
             ;;
             (mask-list (len gtype)		; <掩码> 用于表示组成员是否应该匹配
               (let (mlist)
                 (dotimes (i len)
                   (let (mask)
                     (dotimes (j len)
                       (setf mask (nconc mask (list (= j i)))))
                     (setf mlist (nconc mlist (list mask)))))
                 (when (eql gtype :<=1)
                   (let (mask)
                     (dotimes (i len)
                       (setf mask (cons nil mask)))
                     (setf mlist (nconc mlist (list mask)))))
                 mlist))
             ;; 根据 <说明符> 列表计算应用标志约束
             ;; 参数：
             ;;	rlist		<说明符> 列表
             ;;	use+		必须启用的应用标志列表
             ;;	use-		必须禁用的应用标志列表
             ;;	match?		<说明符> 列表必须匹配(t)还是必须不匹配(nil)
             ;; 返回两个值：* 必须启用的应用标志列表组成的列表
             ;;  * 必须禁用的应用标志列表组成的列表。二者对应顺序保持一致。
             ;; 如果 required-use 中有不在 iuse-effective 中的标志，
             ;;  则第一个返回值是 :not-effective，第二个返回值是标志名称。
             ;; 如果有某个应用标志不能匹配，则第一个返回值是 :not-matched，
             ;;  第二个返回值是标志名称。
             (use-state-constraints (rlist use+ use- &optional (match? t))
               (unless rlist
                 (return-from use-state-constraints (values (list use+) (list use-))))
               (let ((spec (car rlist))
                     global-ulist+ global-ulist-
                     head-ulist+ head-ulist- mismatched-flag)
                 (case (car spec)
                   ((t)
                     (let* ((enable? (char/= (char (cdr spec) 0) #\!))
                            (flag (if enable? (cdr spec) (subseq (cdr spec) 1))))
                       (unless (find flag iuse-effective :test #'string=)
                         (return-from use-state-constraints (values :not-effective flag)))
                       (if (eql enable? match?)	; 同或
                         (if (find flag use- :test #'string=)
                           (return-from use-state-constraints (values :not-matched flag))
                           (setf head-ulist+ (list (if (find flag use+ :test #'string=)
                                                     use+
                                                     (cons flag use+)))
                                 head-ulist- (list use-)))
                         (if (find flag use+ :test #'string=)
                           (return-from use-state-constraints (values :not-matched flag))
                           (setf head-ulist+ (list use+)
                                 head-ulist- (list (if (find flag use- :test #'string=)
                                                     use-
                                                     (cons flag use-))))))))
                   (:all
                     (multiple-value-bind (ulist+ ulist-) (use-state-constraints
                                                            (split-required-use (cdr spec))
                                                            use+
                                                            use-
                                                            match?)
                       (if (listp ulist+)
                         (setf head-ulist+ ulist+
                               head-ulist- ulist-)
                         (return-from use-state-constraints (values ulist+ ulist-)))))
                   (:any
                     (multiple-value-bind (ulist+ ulist-) (anyof-group-constraints
                                                            (split-required-use (cdr spec))
                                                            (list use+)
                                                            (list use-)
                                                            match?)
                       (if (listp ulist+)
                         (setf head-ulist+ ulist+
                               head-ulist- ulist-)
                         (return-from use-state-constraints (values ulist+ ulist-)))))
                   ((:=1 :<=1)
                     (let* ((group-rlist (split-required-use (cdr spec)))
                            (len (length group-rlist))
                            (mlist (mask-list len (car spec)))
                            group-ulist+ group-ulist-)
                       ;; 遍历 <掩码> 列表
                       (do* ((i 0 (1+ i))
                             (mask (nth i mlist) (nth i mlist))
                             (combo-ulist+ (list use+) (list use+))
                             (combo-ulist- (list use-) (list use-)))
                         ((null mask))
                         ;; 根据 <掩码> 遍历组成员
                         (do ((j 0 (1+ j))
                              (node-ulist+ nil nil)
                              (node-ulist- nil nil)
                              (combo-ulist-len (length combo-ulist+) (length combo-ulist+)))
                           ((>= j len))
                           (do ((k 0 (1+ k)))
                             ((>= k combo-ulist-len))
                             (multiple-value-bind (ulist+ ulist-) (use-state-constraints
                                                                    (list (nth j group-rlist))
                                                                    (nth k combo-ulist+)
                                                                    (nth k combo-ulist-)
                                                                    (eql (nth i mask) match?))
                               (if (listp ulist+)
                                 (setf node-ulist+ (nconc node-ulist+ ulist+)
                                       node-ulist- (nconc node-ulist- ulist-))
                                 ;; 错误处理{
                                 (if (eql ulist+ :not-effective)
                                   (return-from use-state-constraints (values :not-effective ulist-))
                                   (setf mismatched-flag ulist-))
                                 ;; }错误处理
                                 )))
                           (unless (setf combo-ulist+ node-ulist+
                                         combo-ulist- node-ulist-)
                             (setf j len)))
                         (setf group-ulist+ (nconc group-ulist+ combo-ulist+)
                               group-ulist- (nconc group-ulist- combo-ulist-)))
                       (setf head-ulist+ group-ulist+
                             head-ulist- group-ulist-)))
                   (t
                     (unless (find (cdar spec) iuse-effective :test #'string=)
                       (return-from use-state-constraints (values :not-effective (cdar spec))))
                     (if (caar spec)
                       ;; 条件组标志不带感叹号{
                       (if (find (cdar spec) use+ :test #'string=)
                         (multiple-value-bind (ulist+ ulist-) (use-state-constraints
                                                                (split-required-use (cdr spec))
                                                                use+
                                                                use-
                                                                match?)
                           (if (listp ulist+)
                             (setf head-ulist+ ulist+
                                   head-ulist- ulist-)
                             (return-from use-state-constraints (values ulist+ ulist-))))
                         (if (find (cdar spec) use- :test #'string=)
                           (setf head-ulist+ (list use+)
                                 head-ulist- (list use-))
                           (multiple-value-bind (ulist+ ulist-) (use-state-constraints
                                                                  (split-required-use (cdr spec))
                                                                  (cons (cdar spec) use+)
                                                                  use-
                                                                  match?)
                             (if (listp ulist+)
                               (setf head-ulist+ (nconc ulist+ (list use+))
                                     head-ulist- (nconc ulist- (list (cons (cdar spec) use-))))
                               (if (eql ulist+ :not-effective)
                                 (return-from use-state-constraints (values :not-effective ulist-))
                                 (setf head-ulist+ (list use+)
                                       head-ulist- (list (cons (cdar spec) use-))))))))
                       ;; }条件组标志不带感叹号
                       ;; 条件组标志带感叹号{
                       (if (find (cdar spec) use- :test #'string=)
                         (multiple-value-bind (ulist+ ulist-) (use-state-constraints
                                                                (split-required-use (cdr spec))
                                                                use+
                                                                use-
                                                                match?)
                           (if (listp ulist+)
                             (setf head-ulist+ ulist+
                                   head-ulist- ulist-)
                             (return-from use-state-constraints (values ulist+ ulist-))))
                         (if (find (cdar spec) use+ :test #'string=)
                           (setf head-ulist+ (list use+)
                                 head-ulist- (list use-))
                           (multiple-value-bind (ulist+ ulist-) (use-state-constraints
                                                                  (split-required-use (cdr spec))
                                                                  use+
                                                                  (cons (cdar spec) use-)
                                                                  match?)
                             (if (listp ulist+)
                               (setf head-ulist+ (nconc ulist+ (list (cons (cdar spec) use+)))
                                     head-ulist- (nconc ulist- (list use-)))
                               (if (eql ulist+ :not-effective)
                                 (return-from use-state-constraints (values :not-effective ulist-))
                                 (setf head-ulist+ (list (cons (cdar spec) use+))
                                       head-ulist- (list use-)))))))
                       ;; }条件组标志带感叹号
                       )))
                 (dotimes (i (length head-ulist+))
                   (multiple-value-bind (ulist+ ulist-) (use-state-constraints
                                                          (cdr rlist)
                                                          (nth i head-ulist+)
                                                          (nth i head-ulist-)
                                                          match?)
                     (if (listp ulist+)
                       (setf global-ulist+ (nconc global-ulist+ ulist+)
                             global-ulist- (nconc global-ulist- ulist-))
                       (if (eql ulist+ :not-effective)
                         (return-from use-state-constraints (values :not-effective ulist-))
                         (setf mismatched-flag ulist-)))))
                 (if global-ulist+
                   (values global-ulist+ global-ulist-)
                   (values :not-matched mismatched-flag)))))
      ;;
      (multiple-value-bind (ulist+ ulist-) (use-state-constraints (split-required-use required-use) use+ use-)
        (if (listp ulist+)
          (mapcar #'car (sort (delete-duplicates (mapcar #'(lambda (u)
                                                             (let ((s (length (intersection u iuse-enable))))
                                                               (cons (sort u #'string<) s)))
                                                         ulist+)
                                                 :test #'equal)
                              #'> :key #'cdr))
          ulist+)))))












